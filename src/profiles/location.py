#!/usr/bin/env python3

#from urllib.request import urlopen
import requests
from bs4 import BeautifulSoup
import twitterscraper
import re
from geopy.geocoders import Nominatim
geolocator = Nominatim(user_agent="sexworke.rs")
from geopy.exc import GeocoderTimedOut
URL_INIT = 'https://twitter.com/'

def parse_url(tweet_user):
    url = URL_INIT+ tweet_user.strip('@')
    return url

def getLocation(user):
    """get geo location of user, from profile"""
    try:
        url = parse_url(user)
        response = requests.get(url)
    except:
        raise
    html = response.text
    soup = BeautifulSoup(html, "lxml")
    #location = soup.find('span','ProfileHeaderCard-locationText').strip('\n').strip()
    location = soup.find('span','ProfileHeaderCard-locationText').string.strip('\n')
    website = soup.find('span','ProfileHeaderCard-urlText').find('a')['title']
    tweets = list(twitterscraper.Tweet.from_html(html))
    #print("website:", website)
    #print("location:", location)
    if location:
        if ',' in location:
            splitted_location = location.split(',')
        else:
            splitted_location = re.split('|;|-|/|°|#', location)
        try:
            if splitted_location:
                located_location = geolocator.geocode(splitted_location[0], timeout=100)
            else:
                located_location = geolocator.geocode(location, timeout=100)
            if located_location:
                user_plus_location = (user, located_location, website, tweets)
                list_of_found_userlocations.append(user_plus_location)
                return user_plus_location
            else:
                user_plus_incorrect_location = (user, location, website, tweets)
                list_of_nonfound_userlocations.append(user_plus_incorrect_location)
                return user_plus_incorrect_location
        except GeocoderTimedOut as e:
            print("Error: geocode failed on input %s with message %s"%(location, e))
            return (user, None, None, None)
