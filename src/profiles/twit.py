#!/usr/bin/env python3

import datetime
import os
import pprint
import sys
import time

import twitterscraper # https://github.com/taspinar/twitterscraper
import location

profiles = (
	'victoriacayne',
	'MistressTissa',
	'LydiaSupremacy',
	'chicagomistress',
	)
def getProfile(user):
	"""Get Profile for user"""
	profile = {
		'location':None,
		'twitterHandle':None,
		'website':None,
		'fullname':None,
		'tweets':(),
		# md format:'date': datetime.datetime.now().strftime("<%Y-%m-%d %a %H:%M>")
		'date': datetime.datetime.now().isoformat()
	}
	loc = location.getLocation(user)
	profile['twitterHandle'] = loc[0]
	profile['location'] = loc[1]
	profile['website'] = loc[2]
	#profile['tweets'] = [o.text for o in loc[3]]
	profile['tweets'] = loc[3]
	profile['fullname'] = loc[3][0].fullname
	#pprint.pprint(profile)
	return profile

def escape(s):
	"""escape for MD"""
	chars = (
		'[',
		']',
		'\\',
		'`',
		'*',
		'_',
		'{',
		'}',
		'(',
		')',
		'#',
		'+',
		'-',
		'.',
		'!',
	)
	for char in chars:
		s = s.replace(char,"\\%s" % char)
	s = s.replace('\n','')
	return s

def createMdDoc(profile):
	"""save profile to Markdown format for hugo/website"""
	md = """---
title: {fullname}
date: {date}
---

## {twitterHandle}

### Website: ({website})
### Location: {location}
### Recent Tweets:

""".format(**profile)

	for tweet in profile['tweets']:
		tweet.text = escape(tweet.text)
		md += "\n##### [{0.text}](https://twitter.com/{0.url})\n".format(tweet)
	return md

def createMdIndex(users):
	"""create index page in md format"""
	d = {
		'date': datetime.datetime.now().isoformat()
		}
	indexmd = """---
title: Profiles
date: {date}
---

""".format(**d)
	for user in users:
		#[below]({{< ref "profiles/victoriacayne.md" >}})
		link = '{{< ref "%s.md" >}}' % user[0]
		text = user[1]
		indexmd += "* [{1}]({0})\n".format(link, text)
	return indexmd

def main():
	"""main"""
	users = []
	for user in profiles:
		profile = getProfile(user)
		md = createMdDoc(profile)
		dirname = os.path.abspath("../../www/content/profiles/")
		fname = os.path.join(dirname,"%s.md" % user)
		with open(fname,'w') as fd:
			fd.write(md)
		users.append((user,profile['fullname']))
	indexMD = createMdIndex(users)
	fname = os.path.join(dirname, "_index.en.md")
	with open(fname,'w') as fd:
		fd.write(indexMD)
	#mainPageFname = os.path.abspath(os.path.join(dirname, "..", "_index.en.md"))
	#mainPage = open(mainPageFname,'r').read()
	#mainPage += '\n' + '\n'.join(indexMD.split('\n')[4:])
	#with open(mainPageFname,'w') as fd:
	#	fd.write(mainPage)

if __name__ == '__main__':
	sys.exit(main())
