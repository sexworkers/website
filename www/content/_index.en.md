---
title: Home
date: <2018-09-12 Wed>
---

This is a public profile site for sexworkers (A public Contacts / Address Book for sex workers).

This site is primarily designed *for* sex workers to find each other, and not as an advertisement platform.
Profiles are meant to be "static" information and not to be changed constantly.

If you want your public sex worker profile added or updated see our [addme]({{ <ref "addme.md"> }}) page.

We respect sex workers, and recognize that sex work is work.
We proudly support sex worker organizations such as these:

#### [Sex Workers Outreach Project](https://www.swopusa.org)
#### [Global Network of Sex Work Projects](https://www.nwsp.org)