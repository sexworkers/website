---
title: Add Me
date: Sun 16 Sep 2018 22:56:15 PDT
---

To add or update your profile send an email to: addme@sexwork.us
The contents of the email should be your profile information, the easiest way to do that is to
copy and paste this into your email to us and fill it out:
```
public_name:
public_email:
twitter_username:
phone:
location:
service_via_internet:
in_person:

```

Notes:
	* The email you send from will not be made public, unless you put it under 'public_email' above, everything else will be made public.
	* service_via_internet is a true/false(boolean) value, if you provide services over the Internet.
	* in_person is a true/false(boolean) value, if you provide in person services (either in call or out call, etc)
	* If you prefer not to answer something, just leave it blank!

Privacy Note:
	Your email address, if not made public, will *ONLY* be used to contact you in regards to your profile,
	however your email address may be sent to police if a legal subpoena is sent to us,
	see our [legal]({{ <ref "legal.md"> }}) page for exact details.

If you find yourself listed and want it removed, please send us an email to removeme@sexwork.us.  If you didn't get added with the same email address as you are sending from, we may require some verification, to ensure you really are the profile "owner", typically this would be sending us a special DM via twitter or something along those lines, it should be very low-barrier and easily accomplished.  Specific details will be done via email.