#+TITLE: Legal
#+DATE: <2018-09-16 Sun>

We do not do any tracking software, run any javascript in your browser, or send any cookies.
This means we are GDPR compliant, since we only store public information basically.

*** All requests by law enforcement:
   - must be a legal subpoena from your local juristdiction.
   - must be sent via email to admin @ this domain.
   - In the english language.

HTTP access logs are normally only written to disk if there was an HTTP or I/O error.
Normal HTTP(200, etc.) traffic is not written to disk. So the chances of us having anything useful
for you is very low, but feel free to request it, and we will do our best to respond in a timely manner.

**** An example HTTP access log (json format):

| key                     | value                          
|-------------------------+--------------------------------|
| time_iso8601":          | "2018-09-16T06:25:16-07:00",   |
| "status":               | "204",                         |
| "bytes_sent":           | "224",                         |
| "http_host":            | "www.sexworke.rs",             |
| "request_time":         | "0.002",                       |
| "http_x_forwarded_for": | "",                            |
| "remote_addr":          | "127.0.0.1",                   |
| "server_addr":          | "127.0.0.1",                   |
| "upstream_addr":        | "127.0.0.1:443",               |
| "request_method":       | "GET",                         |
| "scheme":               | "https",                       |
| "host":                 | "www.sexworke.rs",             |
| "uri":                  | "/en/index.html",              |
| "query_string":         | "?",                           |
| "server_protocol":      | "HTTP/1.1",                    |
| "ssl_protocol":         | "TLSv1.2",                     |
| "ssl_cipher":           | "ECDHE-RSA-AES256-GCM-SHA384", |
| "http_referer":         | "",                            |
| "http_user_agent":      | "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:61.0) Gecko/20100101 Firefox/61.0", |
| "gzip_ratio":           | "",                            |
| "http_cookie":          | ""                             |

**** Email address:
     If someone added themselves via email, then we will store their email address.
     Email contents will either be public(via their profile page) or be deleted once read.
     We do not regularly store email messages sent to this domain once read/sent.
     The email address is available via a legal subpeona if we have it.

No other information is routinely collected.  When debugging or troubleshooting, we reserve the right to collect all information sent to us,
however this data is routinely deleted after serving it's purpose in troubleshooting an error with the site.

Questions about our legal/privacy stance can be sent via email to admin @ this domain.
